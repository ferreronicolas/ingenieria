$(document).ready(function() {
  $("#btnBuscarComercio").click(function() {buscarComercio()});

});
var map = null;
var clustercito = null;
var contAvisos;
var pixeles;

function bootstrap() {

  map = createMap('divMapa', Config.ungsLocation);
  clustercito = L.markerClusterGroup();
  map.layersControl.remove();

}

function buscarComercio(){

  resetVars();

  ciclarComerciosYCategorias(agregarMarkersYDivAvisos);
  ciclarComerciosYCategorias(agregarContenidoAvisos);

  if (clustercito.getLayers().length != 0){

    $("#divAvisos").css("opacity", "1");

  }else{

    $("#divAvisos").css("opacity", "0.3");
    $("#divAvisos").css("height", "572px");

  }

}

function ciclarComerciosYCategorias(tarea){

  var busqueda = $("#txtBusquedaComercio").val().toLowerCase();

  comercios.forEach(ciclarComercios)

  function ciclarComercios(comercio){

    comercio.categorias.forEach(function (categoria){

      if (categoria === busqueda){

        tarea(comercio);

      }

    });

  }

}

function agregarMarkersYDivAvisos(comercio){

  var newMarker = L.marker(comercio.ubicacion, {title: comercio.nombre}).bindPopup("<b>" + comercio.nombre + "</b><br><br>Horarios:" + comercio.horario);
  newMarker.on("click", mostrarAvisosDelComercio);
  newMarker.on("mouseover", onMouseOver);
  newMarker.on("mouseout", onMouseOut);
  map.panTo(new L.LatLng(comercio.ubicacion[0], comercio.ubicacion[1]));

  clustercito.addLayer(newMarker);

  comercio.avisos.forEach(function (aviso){

    if(aviso.tipo === "Venta Directa"){

      pixeles += 270;
      $("#divAvisos").append('<div class="aviso"> <h3 class="tituloAviso"></h3> <p class="comercioAviso"></p> <p class="telefonoAviso"></p> <p class="precioAviso"></p> <p class="detalleAviso"></p> <button class"botonPedir" type="button">Pedir</button><hr></div>');

    }else{

      pixeles += 240;
      $("#divAvisos").append('<div class="aviso"> <h3 class="tituloAviso"></h3> <p class="comercioAviso"></p>  <p class="telefonoAviso"></p> <p class="precioAviso"></p> <p class="detalleAviso"></p> <hr></div>');

    }

    $("#divAvisos").css("height", pixeles+"px");

  });

}

function agregarContenidoAvisos(comercio){

  var listaTitulos = $("h3.tituloAviso");
  var listaComercios = $("p.comercioAviso");
  var listaTelefonos = $("p.telefonoAviso");
  var listaPrecios = $("p.precioAviso");
  var listaDetalles = $("p.detalleAviso");
  var listaBotones = $('[type=button]');

  comercio.avisos.forEach(function (aviso){

    var precio = aviso.precio.toString();

    listaTitulos[contAvisos].innerHTML = aviso.titulo + "<br>";
    listaComercios[contAvisos].innerHTML = "Comercio: " + aviso.comercio.nombre + "<br>";
    listaTelefonos[contAvisos].innerHTML = "Telefono: " + aviso.comercio.telefono + "<br>";
    listaPrecios[contAvisos].innerHTML = "Precio: $" + precio + "<br>";
    listaDetalles[contAvisos].innerHTML = aviso.detalle;

    if(aviso.tipo === "Venta Directa"){

      listaBotones[contAvisos].value = aviso.titulo;
      listaBotones[contAvisos].onclick = function() {pasarVariables("datosCliente.html", ["titulo="+aviso.titulo, "comercio="+"Comercio: "+aviso.comercio.nombre, "precio="+"Precio: $"+precio])};

    }

    contAvisos += 1;

  });

}

function pasarVariables(pagina, nombres) {
  pagina +="?";

  for (i=0; i<nombres.length; i++)
    pagina += nombres[i] + "&";

  pagina = pagina.substring(0,pagina.length-1);
  window.location.href=pagina;
}

function mostrarAvisosDelComercio(e){

  resetVars();

  var tituloComercio = e.target.options.title;

  comercios.forEach(ciclarComercios)

    function ciclarComercios(comercio){

      if(comercio.nombre === tituloComercio){

        agregarMarkersYDivAvisos(comercio);
        agregarContenidoAvisos(comercio);

      }

    }

}

function resetVars(){

  $("#divAvisos").html("<h2 id=tituloAvisos>Avisos Encontrados</h2><hr>");
  contAvisos = 0;
  pixeles = 70;
  clustercito.clearLayers();
  clustercito.addTo(map);

}

function onMouseOver(e) {
  this.openPopup();
};

function onMouseOut(e) {
  this.closePopup();
};
