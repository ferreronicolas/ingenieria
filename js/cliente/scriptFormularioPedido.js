String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.split(search).join(replacement);
};

function bootstrap(){

  cadVariables = location.search.substring(1,location.search.length);
  arrVariables = cadVariables.split("&");

  for (i=0; i<arrVariables.length; i++) {

    arrVariableActual = arrVariables[i].split("=");

    $("#divInfoAviso").append(arrVariableActual[1] + "<br>");

  }

  $("#divInfoAviso").html($("#divInfoAviso").html().replaceAll("%20", " "));

  $("form").submit(function() {
    location.href="uiCliente.html";
  });

}
