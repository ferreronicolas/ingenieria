$(document).ready(function() {
  $("#btnBuscarPedido").click(function() {checkCampos()});
});

function checkCampos(){

  var numeroTelefono = $("#txtNumeroTelefono").val();
  var numeroPedido = $("#txtNumeroPedido").val();

  pedidos.forEach(function (pedido){

    if(pedido.numero.toString() === numeroPedido && pedido.cliente.telefono === numeroTelefono){

      $("#divPedidoEncontrado1").html("Fecha: " + pedido.fecha + "<br>Precio: $" + pedido.aviso.precio);
      $("#divPedidoEncontrado2").html("Comercio: " + pedido.aviso.comercio.nombre + "<br>Producto: " + pedido.aviso.titulo + "<br><br>Estado: " + pedido.estado);

      if(pedido.estado === "Aceptado" || pedido.estado === "Nuevo"){

        $("#divPedidoEncontrado1").append('<br><br><button id="botonCancelar" type="button">Cancelar</button>');
        $("#botonCancelar").click(function() {cancelar(pedido)});

      }else if(pedido.estado === "Entregado"){

        $("#divPedidoEncontrado1").append('<br><br><form id="formCalificar" action="calificacion.html" method="post"><button id="botonCalificar" type="button">Calificar</button></form>');
        $("#botonCalificar").click(function() {calificar()});

      }

    }

  });

}

function calificar() {

  $("#formCalificar").submit();

}

function cancelar(pedido){

  $("#botonCancelar").prop("disabled", true);
  $("#divPedidoEncontrado2").html("Comercio: " + pedido.aviso.comercio.nombre + "<br>Producto: " + pedido.aviso.titulo + "<br><br>Estado: Cancelado");

}
