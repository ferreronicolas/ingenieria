//HARDCODE avisos comercios clientes pedidos

function Comercio(){

  this.id = null;
  this.nombre = "";
  this.ubicacion = [];
  this.categorias = [];
  this.avisos = [];
  this.telefono = "";
  this.horario = "";

}

var stockCenter = new Comercio;
stockCenter.id = 1;
stockCenter.nombre = "Stock Center";
stockCenter.ubicacion = [-34.543592, -58.710024];
stockCenter.categorias = ["calzado", "ropa", "pelotas", "indumentaria", "zapatillas"];
stockCenter.telefono = "17261412";
stockCenter.horario = "Lunes a Viernes de 8hs a 12hs y de 16hs a 20hs. Sabados de 8hs a 16hs."

var ungs = new Comercio;
ungs.id = 2;
ungs.nombre = "Universidad Nacional de General Sarmiento";
ungs.ubicacion = [-34.5213293, -58.7008651]
ungs.categorias = ["libros", "fotocopias", "cuadernillos", "lapiceras", "utiles"]
ungs.telefono = "44697795";
ungs.horario = "Lunes a Sabados de 8hs a 22hs."


var fabricaDePizzas = new Comercio;
fabricaDePizzas.id = 3;
fabricaDePizzas.nombre = "Fabrica de Pizzas";
fabricaDePizzas.ubicacion = [-34.540573, -58.710371];
fabricaDePizzas.categorias = ["comidas", "pizzas", "empanadas"];
fabricaDePizzas.telefono = "43021200";
fabricaDePizzas.horario = "Lunes a Sabados de 11hs a 00hs."


var soloDeportes = new Comercio;
soloDeportes.id = 4;
soloDeportes.nombre = "Solo Deportes"
soloDeportes.ubicacion = [-34.544378, -58.709149];
soloDeportes.categorias = ["calzado", "ropa", "pelotas", "indumentaria", "zapatillas","futbol","botines", "deportes"];
soloDeportes.telefono = "65587321";
soloDeportes.horario = "Lunes a Viernes de 10hs a 13hs y de 16hs a 20hs. Sabados de 12hs a 21hs."


var jardineriaPepe = new Comercio;
jardineriaPepe.id = 5;
jardineriaPepe.nombre = "Jardineria Pepe";
jardineriaPepe.ubicacion = [-34.504393, -58.694763];
jardineriaPepe.categorias = ["jardineria", "plantas", "cesped"];
jardineriaPepe.telefono = "48985112";
jardineriaPepe.horario = "Lunes a Viernes de 8hs  a 20hs. Sabados de 8hs a 16hs."


function Aviso(){

  this.id = null;
  this.titulo = "";
  this.detalle = "";
  this.precio = 0;
  this.categorias = "";
  this.comercio = new Comercio();
  this.tipo = "";

}

var aviso1 = new Aviso;
aviso1.id = 1;
aviso1.titulo = "Zapatillas Urbanas New Balance Mujer 574 Camufladas Moda";
aviso1.detalle = "La capellada cuenta con una combinación de cuero descarne y textil, y una suela de goma muy duradera.";
aviso1.precio = 3339;
aviso1.categorias = "zapatillas";
aviso1.comercio = stockCenter;
aviso1.tipo = "Venta";

var aviso2 = new Aviso;
aviso2.id = 2;
aviso2.titulo = "Docena de Empanadas";
aviso2.detalle = "Gustos: Carne, Jamon y Queso, Humita, Carne Picante, Carne a Cuchillo, Queso y Cebolla";
aviso2.precio = 200;
aviso2.categorias = "empanadas";
aviso2.comercio = fabricaDePizzas;
aviso2.tipo = "Venta Directa";

var aviso3 = new Aviso;
aviso3.id = 3;
aviso3.titulo = "Campera Le Coq Sportif Puff Light Reversible Hombres";
aviso3.detalle = "Campera de abrigo en tejido poleister color verde militar reversible con estampa camuflada y con relleno guata 380p/c.";
aviso3.precio = 2869;
aviso3.categorias = "ropa";
aviso3.comercio = stockCenter;
aviso3.tipo = "Venta";

var aviso4 = new Aviso;
aviso4.id = 4;
aviso4.titulo = "Pizza de Muzarella";
aviso4.detalle = "Pizzas baratas y safables";
aviso4.precio = 90;
aviso4.categorias = "pizzas";
aviso4.comercio = fabricaDePizzas;
aviso4.tipo = "Venta Directa";

var aviso5 = new Aviso;
aviso5.id = 5;
aviso5.titulo = "Pizza Especial";
aviso5.detalle = "Pizzas baratas y safables. Sabores: Jamon, Jamon y morron, Huevo, Salchicha, Calabresa, Anana";
aviso5.precio = 120;
aviso5.categorias = "pizzas";
aviso5.comercio = fabricaDePizzas;
aviso5.tipo = "Venta Directa";

var aviso6 = new Aviso;
aviso6.id = 6;
aviso6.titulo = "Camiseta C.A. Independiente alternativa";
aviso6.detalle = "Esta es la camiseta alternativa del Club Atlético Independiente para hombre. Tecnología Dry Cell, control de humedad que mantiene el cuerpo seco durante el ejercicio. Logo Puma y escudo CAI estampado.";
aviso6.precio = 1899;
aviso6.categorias = "indumentaria";
aviso6.comercio = soloDeportes;
aviso6.tipo = "Venta";

var aviso7 = new Aviso;
aviso7.id = 7;
aviso7.titulo = "Zapatillas Urbanas New Balance 215 MR";
aviso7.detalle = "Las zapatillas 215 RR de New Balance están confeccionadas con un diseño moderno, y una suela liviana. Ideales para tus actividades urbanas.";
aviso7.precio = 2.999;
aviso7.categorias = "zapatillas";
aviso7.comercio = soloDeportes;
aviso7.tipo = "Venta";

var aviso8 = new Aviso;
aviso8.id = 8;
aviso8.titulo = "Servicio De Jardineria ";
aviso8.detalle = "Paisajismo / Poda / Colocacion Cesped / Mantenimiento / Precio a convenir";
aviso8.precio = 0;
aviso8.categorias = "jardineria";
aviso8.comercio = jardineriaPepe;
aviso8.tipo = "Servicio";

function Cliente() {

  this.id = null;
  this.nombre = "";
  this.direccion = [];
  this.telefono = "";
  this.mail = "";

}

var cliente1 = new Cliente;
cliente1.id = 1;
cliente1.nombre = "Paola";
cliente1.direccion = ["Calle falsa 123", ""]
cliente1.telefono = "56824879";
cliente1.mail = "asdasd@gmail.com";

var cliente2 = new Cliente;
cliente2.id = 2;
cliente2.nombre = "Fercho";
cliente2.direccion = ["Calle falsa 456", ""]
cliente2.telefono = "12345678";
cliente2.mail = "jahsgdjka@gmail.com";

var cliente3 = new Cliente;
cliente3.id = 3;
cliente3.nombre = "Iara";
cliente3.direccion = ["Calle falsa 789", ""]
cliente3.telefono = "94824796";
cliente3.mail = "zxcnzmxcd@gmail.com";

function Pedido() {

  this.id = null;
  this.numero = -1;
  this.fecha = "";
  this.cliente = new Cliente();
  this.aviso = new Aviso();
  this.estado = "";

}

var pedido1 = new Pedido;
pedido1.numero = 1;
pedido1.fecha = "01/11/18";
pedido1.cliente = cliente1;
pedido1.aviso = aviso2;
pedido1.estado = "Aceptado";

var pedido2 = new Pedido;
pedido2.numero = 2;
pedido2.fecha = "10/09/18";
pedido2.cliente = cliente2;
pedido2.aviso = aviso5;
pedido2.estado = "Entregado";

var pedido3 = new Pedido;
pedido3.numero = 3;
pedido3.fecha = "01/11/18";
pedido3.cliente = cliente2;
pedido3.aviso = aviso4;
pedido3.estado = "Despachado";

var comercios = [];
comercios.push(stockCenter);
comercios.push(ungs);
comercios.push(fabricaDePizzas);
comercios.push(soloDeportes);
comercios.push(jardineriaPepe);
stockCenter.avisos.push(aviso1);
stockCenter.avisos.push(aviso3);
fabricaDePizzas.avisos.push(aviso2);
fabricaDePizzas.avisos.push(aviso4);
fabricaDePizzas.avisos.push(aviso5);
soloDeportes.avisos.push(aviso6);
soloDeportes.avisos.push(aviso7);
jardineriaPepe.avisos.push(aviso8);

var clientes = [];
clientes.push(cliente1);
clientes.push(cliente2);
clientes.push(cliente3);

var pedidos = [];
pedidos.push(pedido1);
pedidos.push(pedido2);
pedidos.push(pedido3);
