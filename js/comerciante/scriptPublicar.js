$(document).ready(function() {
  $("#btnProductosSimilares").click(function() {productosSimilares()});
  $("input:radio[name=tipoDeVenta]").change(function() {checkPrecioRequerido(this)});
  $("#txtPrecio").focusout(function() {validar()})
  $("form").submit(function () {
    alert("Aviso publicado con éxito.");
  });
});

function checkPrecioRequerido(radio) {
  if ( radio.checked && radio.value == "radioVentaDirecta" ) {
    $("#txtPrecio").prop("required", true);
  } else {
    $("#txtPrecio").prop("required", false);
  }

  if ( radio.checked && radio.value == "radioServicio" ) {
    $("#divLimitaciones").css("display", "block");
    $("#txtLimitaciones").prop("required", true);
  } else {
    $("#divLimitaciones").css("display", "none");
    $("#txtLimitaciones").prop("required", false);
  }
};

function productosSimilares() {
  var busqueda = $("#txtNombreAviso").val();
  var resObj = $.ajax(Config.linkMercadoLibre + busqueda)
                  .done( function() {mostrarRespuesta(resObj.responseJSON)} );
};

function mostrarRespuesta(respuesta) {
  if ( respuesta.results.length == 0 ) {
    alert("No hubo resultados.")
  }else {
    var imagenes = $("img.imagenProducto");
    var precios = $("p.precioProducto");
    var titulos = $("p.tituloProducto");
    for (var i = 0; i < imagenes.length; i++) {
      imagenes[i].src = respuesta.results[i].thumbnail;
      precios[i].innerHTML = "Precio: $" + respuesta.results[i].price;
      titulos[i].innerHTML = respuesta.results[i].title;
    }
  }
};

function validar() {
    var input = $("#txtPrecio")[0];
    var parrafoMensaje = $("#validationMessage");
    if (!input.checkValidity()) {
      parrafoMensaje.html("El número debe tener dos dígitos después de la coma.");
    } else {
      parrafoMensaje.html("");
    }
}
